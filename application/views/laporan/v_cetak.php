<div class="row">
	<div class="col-md-12">
		<form action="app/cetak" method="POST">
			<div class="form-group">
				<label>Dari Tanggal</label>
				<input type="date" name="tgl1" class="form-control">
			</div>
			<div class="form-group">
				<label>Sampai Tanggal</label>
				<input type="date" name="tgl2" class="form-control">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary"> <i class="fa fa-print"></i> Cetak</button>
			</div>
		</form>
	</div>
</div>