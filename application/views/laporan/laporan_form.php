
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="date">Tgl Laporan <?php echo form_error('tgl_laporan') ?></label>
            <input type="text" class="form-control" name="tgl_laporan" id="tgl_laporan" placeholder="Tgl Laporan" value="<?php echo $tgl_laporan; ?>" />
        </div>
	    <div class="form-group">
            <label for="uraian_kegiatan">Uraian Kegiatan </label>
            <textarea class="form-control" rows="3" name="uraian_kegiatan" id="uraian_kegiatan" placeholder="Uraian Kegiatan"><?php echo $uraian_kegiatan; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Tahapan </label>
            <!-- <input type="text" class="form-control" name="tahapan" id="tahapan" placeholder="Tahapan" value="<?php echo $tahapan; ?>" /> -->
            <textarea class="form-control" rows="3" name="tahapan" id="tahapan" placeholder="Tahapan"><?php echo $tahapan; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Pihak Hadir </label>
           <textarea class="form-control" rows="3" name="pihak_hadir" id="pihak_hadir" placeholder="Pihak Hadir"><?php echo $pihak_hadir; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Target </label>
            <textarea class="form-control" rows="3" name="target" id="target" placeholder="Target"><?php echo $target; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="dugaan_pelanggaran">Dugaan Pelanggaran <?php echo form_error('dugaan_pelanggaran') ?></label>
            <textarea class="form-control" rows="3" name="dugaan_pelanggaran" id="dugaan_pelanggaran" placeholder="Dugaan Pelanggaran"><?php echo $dugaan_pelanggaran; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Upload Dokumen Asli </label>
            <input type="file" class="form-control" name="foto" id="foto" />

            <input type="hidden" name="foto_old" value="<?php echo $foto ?>">
            <div>
                <?php if ($foto != ''): ?>
                    <b>*) Foto Sebelumnya : </b><br>
                    <img src="image/file/<?php echo $foto ?>" style="width: 100px;">
                <?php endif ?>
            </div>
        </div>
	    
	    <input type="hidden" name="id_laporan" value="<?php echo $id_laporan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('laporan') ?>" class="btn btn-default">Cancel</a>
	</form>
   
   <script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>
   <link href="https://repo.rachmat.id/jquery-ui-1.12.1/jquery-ui.css" rel="stylesheet">
    <script type="text/javascript" src="https://repo.rachmat.id/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://repo.rachmat.id/jquery-ui-1.12.1/jquery-ui.js"></script>
   <script type="text/javascript">
       CKEDITOR.replace( 'uraian_kegiatan' );
       CKEDITOR.replace( 'tahapan' );
       CKEDITOR.replace( 'pihak_hadir' );
       CKEDITOR.replace( 'target' );
       CKEDITOR.replace( 'dugaan_pelanggaran' );

       $(function(){
          $("#tgl_laporan").datepicker({
             dateFormat:"yy-mm-dd",
          });
        });
   </script>