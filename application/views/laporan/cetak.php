<!DOCTYPE html>
<html>
<head>
	<title>Laporan</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body onload="print()">

<center>
	<h2>Rekap Laporan</h2>
</center>
<hr>

<b>Dari : <?php echo $tgl1 ?> - <?php echo $tgl2 ?></b>

<table class="table table-bordered">
	            <tr>
                <th>No</th>
		<th>Tgl Laporan</th>
		<th>Uraian Kegiatan</th>
		<th>Tahapan</th>
		<th>Pihak Hadir</th>
		<th>Target</th>
		<th>Dugaan Pelanggaran</th>
		<th>Created At</th>
		<th>User Create</th>
            </tr><?php
            $start = 1;
           
            foreach ($laporan_data->result() as $laporan)
            {
                ?>
                <tr>
			<td width="80px"><?php echo $start ?></td>
			<td><?php echo $laporan->tgl_laporan ?></td>
			<td><?php echo $laporan->uraian_kegiatan ?></td>
			<td><?php echo $laporan->tahapan ?></td>
			<td><?php echo $laporan->pihak_hadir ?></td>
			<td><?php echo $laporan->target ?></td>
			<td><?php echo $laporan->dugaan_pelanggaran ?></td>
			
			<td><?php echo $laporan->created_at ?></td>
			<td><?php echo get_data('a_user','id_user',$laporan->user_create,'nama_lengkap') ?>  - <b><?php echo get_data('a_user','id_user',$laporan->user_create,'level'); ?></b></td>
			
		</tr>
                <?php
                $start++;
            }
            ?>
</table>


</body>
</html>