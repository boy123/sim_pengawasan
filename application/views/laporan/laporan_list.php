
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('laporan/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('laporan/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('laporan'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Tgl Laporan</th>
		<th>Uraian Kegiatan</th>
		<th>Tahapan</th>
		<th>Pihak Hadir</th>
		<th>Target</th>
		<th>Dugaan Pelanggaran</th>
		<th>Foto</th>
		<th>Created At</th>
		<th>User Create</th>
		<th>Action</th>
            </tr><?php
            $start = 1;
            $id_user = $this->session->userdata('id_user');
            if ($this->session->userdata('level') == 'admin') {
                $laporan_data = $this->db->get('laporan');
            } else {
                $laporan_data = $this->db->get_where('laporan', array('user_create'=>$id_user));
            }
           
            foreach ($laporan_data->result() as $laporan)
            {
                ?>
                <tr>
			<td width="80px"><?php echo $start ?></td>
			<td><?php echo $laporan->tgl_laporan ?></td>
			<td><?php echo $laporan->uraian_kegiatan ?></td>
			<td><?php echo $laporan->tahapan ?></td>
			<td><?php echo $laporan->pihak_hadir ?></td>
			<td><?php echo $laporan->target ?></td>
			<td><?php echo $laporan->dugaan_pelanggaran ?></td>
			<td>
                <a href="image/file/<?php echo $laporan->foto ?>" target="_blank">
                <img src="image/file/<?php echo $laporan->foto ?>" style="width: 100px; height: 100px;">
                </a>        
            </td>
			<td><?php echo $laporan->created_at ?></td>
			<td><?php echo get_data('a_user','id_user',$laporan->user_create,'nama_lengkap') ?>  - <b><?php echo get_data('a_user','id_user',$laporan->user_create,'level'); ?></b></td>
			<td style="text-align:center" width="200px">
				<?php 
                echo anchor(site_url('laporan/read/'.$laporan->id_laporan),'<span class="label label-primary">Lihat</span>'); 
                echo ' | '; 
				echo anchor(site_url('laporan/update/'.$laporan->id_laporan),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('laporan/delete/'.$laporan->id_laporan),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
                $start++;
            }
            ?>
        </table>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    