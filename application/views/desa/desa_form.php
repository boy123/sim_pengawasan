
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Kecamatan <?php echo form_error('id_kecamatan') ?></label>
            <select class="form-control select2" name="id_kecamatan" required="">
                <option value="<?php echo get_data('kecamatan','id_kecamatan',$id_kecamatan,'kecamatan') ?>"><?php echo get_data('kecamatan','id_kecamatan',$id_kecamatan,'kecamatan') ?></option>
                <?php foreach ($this->db->get('kecamatan')->result() as $rw) {
                ?>
                <option value="<?php echo $rw->id_kecamatan ?>"><?php echo $rw->kecamatan ?></option>
                <?php   
                } ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">Desa <?php echo form_error('desa') ?></label>
            <input type="text" class="form-control" name="desa" id="desa" placeholder="Desa" value="<?php echo $desa; ?>" />
        </div>
	    <input type="hidden" name="id_desa" value="<?php echo $id_desa; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('desa') ?>" class="btn btn-default">Cancel</a>
	</form>
   