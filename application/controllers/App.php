<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	public $image = '';
	
	public function index()
	{
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
		$data = array(
			'konten' => 'home_admin',
            'judul_page' => 'Dashboard',
		);
		$this->load->view('v_index', $data);
    }

    public function cetak()
    {
    	if ($_POST) {
    		$tgl1 = $this->input->post('tgl1');
    		$tgl2 = $this->input->post('tgl2');

    		$sql = $this->db->query("
    			SELECT * FROM laporan WHERE tgl_laporan BETWEEN '$tgl1' and '$tgl2' order by id_laporan DESC;
    			");
    		$data = array(
    			'tgl1' => $tgl1,
    			'tgl2' => $tgl2,
    			'laporan_data' => $sql
    		);
    		$this->load->view('laporan/cetak', $data);
    	} else {
    		$data = array(
				'konten' => 'laporan/v_cetak',
	            'judul_page' => 'Cetak Laporan',
			);
			$this->load->view('v_index', $data);
    	}
    }

   

   
	

	

	
}
