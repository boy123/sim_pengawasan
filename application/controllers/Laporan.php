<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Laporan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'laporan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'laporan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'laporan/index.html';
            $config['first_url'] = base_url() . 'laporan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Laporan_model->total_rows($q);
        $laporan = $this->Laporan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'laporan_data' => $laporan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'laporan/laporan_list',
            'konten' => 'laporan/laporan_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Laporan_model->get_by_id($id);
        if ($row) {
            $data = array(
                'judul_page' => 'Detail Laporan',
            'konten' => 'laporan/laporan_read',
		'id_laporan' => $row->id_laporan,
		'tgl_laporan' => $row->tgl_laporan,
		'uraian_kegiatan' => $row->uraian_kegiatan,
		'tahapan' => $row->tahapan,
		'pihak_hadir' => $row->pihak_hadir,
		'target' => $row->target,
		'dugaan_pelanggaran' => $row->dugaan_pelanggaran,
		'foto' => $row->foto,
		'created_at' => $row->created_at,
		'user_create' => $row->user_create,
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('laporan'));
        }
    }

    public function create() 
    {

        $data = array(
            'judul_page' => 'laporan/laporan_form',
            'konten' => 'laporan/laporan_form',
            'button' => 'Create',
            'action' => site_url('laporan/create_action'),
	    'id_laporan' => set_value('id_laporan'),
	    'tgl_laporan' => set_value('tgl_laporan'),
	    'uraian_kegiatan' => set_value('uraian_kegiatan'),
	    'tahapan' => set_value('tahapan'),
	    'pihak_hadir' => set_value('pihak_hadir'),
	    'target' => set_value('target'),
	    'dugaan_pelanggaran' => set_value('dugaan_pelanggaran'),
	    'foto' => set_value('foto'),
	    'created_at' => get_waktu(),
	    'user_create' => $this->session->userdata('id_user'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

             $img = upload_gambar_biasa('user', 'image/file/', 'jpg|png|jpeg', 10000, 'foto');
            $data = array(
		'tgl_laporan' => $this->input->post('tgl_laporan',TRUE),
		'uraian_kegiatan' => $this->input->post('uraian_kegiatan',TRUE),
		'tahapan' => $this->input->post('tahapan',TRUE),
		'pihak_hadir' => $this->input->post('pihak_hadir',TRUE),
		'target' => $this->input->post('target',TRUE),
		'dugaan_pelanggaran' => $this->input->post('dugaan_pelanggaran',TRUE),
		'foto' => $img,
		'created_at' => get_waktu(),
		'user_create' => $this->session->userdata('id_user'),
	    );

            $this->Laporan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('laporan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Laporan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'laporan/laporan_form',
                'konten' => 'laporan/laporan_form',
                'button' => 'Update',
                'action' => site_url('laporan/update_action'),
		'id_laporan' => set_value('id_laporan', $row->id_laporan),
		'tgl_laporan' => set_value('tgl_laporan', $row->tgl_laporan),
		'uraian_kegiatan' => set_value('uraian_kegiatan', $row->uraian_kegiatan),
		'tahapan' => set_value('tahapan', $row->tahapan),
		'pihak_hadir' => set_value('pihak_hadir', $row->pihak_hadir),
		'target' => set_value('target', $row->target),
		'dugaan_pelanggaran' => set_value('dugaan_pelanggaran', $row->dugaan_pelanggaran),
		'foto' => set_value('foto', $row->foto),
		'created_at' => set_value('created_at', $row->created_at),
		'user_create' => set_value('user_create', $row->user_create),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('laporan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_laporan', TRUE));
        } else {
            $data = array(
		'tgl_laporan' => $this->input->post('tgl_laporan',TRUE),
		'uraian_kegiatan' => $this->input->post('uraian_kegiatan',TRUE),
		'tahapan' => $this->input->post('tahapan',TRUE),
		'pihak_hadir' => $this->input->post('pihak_hadir',TRUE),
		'target' => $this->input->post('target',TRUE),
		'dugaan_pelanggaran' => $this->input->post('dugaan_pelanggaran',TRUE),
		'foto' => $retVal = ($_FILES['foto']['name'] == '') ? $_POST['foto_old'] : upload_gambar_biasa('user', 'image/file/', 'jpeg|png|jpg|gif', 10000, 'foto'),
		'created_at' => get_waktu(),
		'user_create' => $this->session->userdata('id_user'),
	    );

            $this->Laporan_model->update($this->input->post('id_laporan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('laporan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Laporan_model->get_by_id($id);

        if ($row) {
            $this->Laporan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('laporan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('laporan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('tgl_laporan', 'tgl laporan', 'trim|required');
	// $this->form_validation->set_rules('uraian_kegiatan', 'uraian kegiatan', 'trim|required');
	// $this->form_validation->set_rules('tahapan', 'tahapan', 'trim|required');
	// $this->form_validation->set_rules('pihak_hadir', 'pihak hadir', 'trim|required');
	// $this->form_validation->set_rules('target', 'target', 'trim|required');
	// $this->form_validation->set_rules('dugaan_pelanggaran', 'dugaan pelanggaran', 'trim|required');

	$this->form_validation->set_rules('id_laporan', 'id_laporan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Laporan.php */
/* Location: ./application/controllers/Laporan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-09-11 08:33:58 */
/* https://jualkoding.com */