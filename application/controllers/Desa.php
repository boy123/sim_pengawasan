<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Desa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Desa_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'desa/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'desa/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'desa/index.html';
            $config['first_url'] = base_url() . 'desa/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Desa_model->total_rows($q);
        $desa = $this->Desa_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'desa_data' => $desa,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'desa/desa_list',
            'konten' => 'desa/desa_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Desa_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_desa' => $row->id_desa,
		'id_kecamatan' => $row->id_kecamatan,
		'desa' => $row->desa,
	    );
            $this->load->view('desa/desa_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('desa'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'desa/desa_form',
            'konten' => 'desa/desa_form',
            'button' => 'Create',
            'action' => site_url('desa/create_action'),
	    'id_desa' => set_value('id_desa'),
	    'id_kecamatan' => set_value('id_kecamatan'),
	    'desa' => set_value('desa'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
		'desa' => $this->input->post('desa',TRUE),
	    );

            $this->Desa_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('desa'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Desa_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'desa/desa_form',
                'konten' => 'desa/desa_form',
                'button' => 'Update',
                'action' => site_url('desa/update_action'),
		'id_desa' => set_value('id_desa', $row->id_desa),
		'id_kecamatan' => set_value('id_kecamatan', $row->id_kecamatan),
		'desa' => set_value('desa', $row->desa),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('desa'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_desa', TRUE));
        } else {
            $data = array(
		'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
		'desa' => $this->input->post('desa',TRUE),
	    );

            $this->Desa_model->update($this->input->post('id_desa', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('desa'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Desa_model->get_by_id($id);

        if ($row) {
            $this->Desa_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('desa'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('desa'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_kecamatan', 'id kecamatan', 'trim|required');
	$this->form_validation->set_rules('desa', 'desa', 'trim|required');

	$this->form_validation->set_rules('id_desa', 'id_desa', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Desa.php */
/* Location: ./application/controllers/Desa.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-09-11 08:33:46 */
/* https://jualkoding.com */